module.exports = (app, graphqlHTTP, graphql, db, studDetails, studRole) => {
    
    const studentData = new graphql.GraphQLObjectType({
        name: 'StudentDetails',
        fields: () => ({
            id: { type: graphql.GraphQLID },
            student_id: { type: graphql.GraphQLID },
            age: { type: graphql.GraphQLString }
        })
    })

    const studentRole = new graphql.GraphQLObjectType({
        name: 'StudentRoles',
        fields: () => ({
            id: { type: graphql.GraphQLID },
            student_id: { type: graphql.GraphQLID },
            role: { type: graphql.GraphQLString }
        })
    })
    
    
    const student = new graphql.GraphQLObjectType({
        name: 'Student',
        fields: () => ({
            id: {type: graphql.GraphQLID},
            dob: { type: graphql.GraphQLString },
            name: {type: graphql.GraphQLString},
            mobile: {type: graphql.GraphQLString},
            password: {type: graphql.GraphQLString},
            studentDetails: {
                type: graphql.GraphQLList(studentData)
            },
            studentRoles: {
                type: graphql.GraphQLList(studentRole) 
            }
        })
    })


    // studDetails.belongsTo(db,  { foreignKey: "student_id", targetKey: "id"
    //     // allowNull: false
    //   } );
    db.hasMany(studDetails, { foreignKey: "student_id" });
    // db.belongsToMany(studDetails, { through: "studentDetails", foreignKey: "student_id", targetKey: "id" });
    studDetails.belongsTo(db, { foreignKey: { name: "student_id" } });

    //   studRole.belongsTo(db, {
    //     foreignKey: "student_id",
    //         // allowNull: false
          
    // });
    db.hasMany(studRole, { foreignKey: "student_id" });
    studRole.belongsTo(db, { foreignKey: { name: "student_id" }})



    const StudentQueryList = new graphql.GraphQLObjectType({
        name: 'GetStudentQuery',
        fields: {
            GetStudent: {
                type: graphql.GraphQLList(graphql.GraphQLNonNull(student)),
                args: { id: { type: graphql.GraphQLID }  },
                async resolve(res, args) {
                    var final
                    if (args == undefined || Object.keys(args).length < 0) {
                        console.log('please enter the required arguments')
                    } else {
                        var id = args.id
                    console.log(id)
                        // var attributes = [['studentDetails.id', 'studDetailId'], ['studentRoles.id', 'studRoleId']]
                        var _dbResult = await db.findAll({ 
                            // raw: true, 
                            // subQuery: true,
                            // attributes: attributes,
                            include: [ { model: studDetails, 
                            // attributes: [ 'age', 'address', 'city', 'state'] 
                        }, 
                            { model: studRole,
                            attributes: [ 'role' ] }] , where: { id: id } })
                        
                        var result = _dbResult 
                        // console.log(result)    
                    }
                    return result
                }
            }
        }
    })
    
    
    var schema = new graphql.GraphQLSchema({
        query: StudentQueryList
    })
    
    
    app.use('/GetStudentDetails', graphqlHTTP({
        schema,
        graphiql: true
    }))


}