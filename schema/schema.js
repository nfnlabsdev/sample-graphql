module.exports = (app, graphqlHTTP, graphql, db) => {




var stuData = [
    {id: 1, name: 'first', mobile: '9876567', password: '12345'},
    {id: 2, name: 'second', mobile: '9876567', password: '12345'},
    {id: 3, name: 'third', mobile: '9876567', password: '12345'},
    {id: 4, name: 'four', mobile: '9876567', password: '12345'},
    {id: 5, name: 'five', mobile: '9876567', password: '12345'},
    {id: 6, name: 'six', mobile: '9876567', password: '12345'}
]



const student = new graphql.GraphQLObjectType({
    name: 'Student',
    fields: () => ({
        id: {type: graphql.GraphQLInt},
        dob: { type: graphql.GraphQLString },
        name: {type: graphql.GraphQLString},
        mobile: {type: graphql.GraphQLString},
        password: {type: graphql.GraphQLString}
    })
})


const studentQuery = new graphql.GraphQLObjectType({
    name: 'StudentQuery',
    fields: {
        Student: {
            type: student,
            args: { id: { type: graphql.GraphQLID }  },
            async resolve(res, args) {
                var id = args.id
                var _dbResult = await db.findAll({  where: { id: id } }).all()
                
                var result = _dbResult[0].dataValues
                // var _res = stuData.map((x) => {
                //     if (x.id == id) {
                //         result = x
                //     }
                // })
                // console.log(result)

                return result
            }
        }
    }
})


var schema = new graphql.GraphQLSchema({
    query: studentQuery
})


app.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true
}))


}

