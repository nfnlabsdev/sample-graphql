module.exports = (app, graphqlHTTP, graphql, db) => {

    
    
    
    const student = new graphql.GraphQLObjectType({
        name: 'Student',
        fields: () => ({
            id: {type: graphql.GraphQLInt},
            dob: { type: graphql.GraphQLString },
            name: {type: graphql.GraphQLString},
            mobile: {type: graphql.GraphQLString},
            password: {type: graphql.GraphQLString}
        })
    })
    
    
    const addStudentQuery = new graphql.GraphQLObjectType({
        name: 'AddStudentQuery',
        fields: {
            StudentAdd: {
                type: student,
                args: { id: { type: graphql.GraphQLID }, name: { type: graphql.GraphQLString }, dob: { type: graphql.GraphQLString }, mobile: { type: graphql.GraphQLString }, password: { type: graphql.GraphQLString }  },
                async resolve(res, args) {
                    var id = args.id
                    console.log(args)
                    var final
                    if (args == undefined || Object.keys(args).length < 0) {
                        console.log('please enter the required arguments')
                    } else {
                        db.create(args).then(async (resp)=> {
                            console.log(resp.id)
                            var _stuResult = await db.findOne({ where: { id: resp.id }})
                            // console.log(_stuResult.dataValues)
                            // return _stuResult.dataValues
                            final = _stuResult.dataValues
                        })      
                    }
                    return final
                }
            },
            StudentUpdate: {
                type: student,
                args: { id: { type: graphql.GraphQLID }, name: { type: graphql.GraphQLString }, dob: { type: graphql.GraphQLString }, mobile: { type: graphql.GraphQLString }, password: { type: graphql.GraphQLString }  },
                async resolve(res, args) {
                    var id = args.id
                    console.log(args)
                    
                    if (args == undefined || Object.keys(args).length < 0) {
                        console.log('please enter the required arguments')
                    } else {
                        db.update(args, { where: { id: id }}).then(async (resp)=> {
                            console.log(resp.id)
                            return 'updated successfully'
                        })      
                    }
                    
                }
            },
            StudentDelete: {
                type: student,
                args: { id: { type: graphql.GraphQLID }  },
                async resolve(res, args) {
                    var id = args.id
                    if (args == undefined || id == null) {
                        console.log('please enter the ID')
                    } else {
                        db.destroy({ where: {
                            id: id
                        }}).then(async (resp)=> {
                            console.log(resp)
                            return 'deleted successfully'
                        })      
                    }
                    
                }
            }
        }
    })
    
    
    var schema = new graphql.GraphQLSchema({
        query: addStudentQuery
    })
    
    
    app.use('/addMutation', graphqlHTTP({
        schema,
        graphiql: true
    }))
}