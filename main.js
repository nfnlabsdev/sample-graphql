const express = require('express');
const graphqlHTTP = require('express-graphql');
const graphiql = require('graphql');
const bodyParser = require('body-parser')
require('dotenv').config()
const  sequelize = require('sequelize');

const dbConfig = new sequelize.Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS,{
  dialect: 'mysql',
  host: process.env.DB_HOST  
})


dbConfig.authenticate().then((error) => {
    if (error) {
        console.log('Error ==>', error)
    } else {
        console.log('connection successfully')
    }
}).catch((err) => {
    console.log(err)
}).finally(() => {
    
})



var Test = dbConfig.define('student', {
    name: {
        type: sequelize.DataTypes.STRING,
        allowNull: true
    },
    dob: {
        type: sequelize.DataTypes.STRING,
        allowNull: true
    },
    mobile: {
        type: sequelize.DataTypes.STRING,
        allowNull: true
    },
    password: {
        type: sequelize.DataTypes.STRING,
        allowNull: true
    }
}, {
  freezeTableName: true,
  tableName: 'student',
  timestamps: false ,
  underscored: true 
});  


var studentData = dbConfig.define('studentDetails', {
    student_id: {
        type: sequelize.DataTypes.INTEGER,
        allowNull: true,
        references: {
            model: Test,
            key: "id"
        }
    },
    age: {
        type: sequelize.DataTypes.STRING,
        allowNull: true
    },
    address: {
        type: sequelize.DataTypes.STRING,
        allowNull: true
    },
    city: {
        type: sequelize.DataTypes.STRING,
        allowNull: true
    },
    state: {
        type: sequelize.DataTypes.STRING,
        allowNull: true
    }
}, {
  freezeTableName: true,
  tableName: 'studentDetails',
  timestamps: false ,
  underscored: true 
});

var studentRole = dbConfig.define('studentRoles', {
    student_id: {
        type: sequelize.DataTypes.INTEGER,
        allowNull: true,
        references: {
            model: Test,
            key: "id"
        }
    },
    role: {
        type: sequelize.DataTypes.STRING,
        allowNull: true
    }
}, {
  freezeTableName: true,
  tableName: 'studentRoles',
  timestamps: false ,
  underscored: true 
});
// dbconnect()



const app = express();
// app.use(bodyParser.json())
// app.use(bodyParser.urlencoded({ extended: true }))


var schema = require('./schema/schema')(app, graphqlHTTP, graphiql, Test)
var mutation = require('./schema/mutation')(app, graphqlHTTP, graphiql, Test)
var modelData = require('./schema/modelList')(app, graphqlHTTP, graphiql, Test, studentData, studentRole)




app.listen(process.env.PORT, () => {
    console.log(`Running server at http://${process.env.HOST}:${process.env.PORT}`);

});